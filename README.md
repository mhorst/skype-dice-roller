# Skype Dice Roller

This bot can sign into Skype and roll dice for you and other members of a chat.

## Getting started

You will need Python3 and a copy of the files in the repository
* Clone the repository, e.g. `git clone git@gitlab.com:mhorst/skype-dice-roller.git`
* Install requirements, e.g. `pip3 install -r requirements.txt`
* Make a copy of the example configuration, e.g. `cp config.example config.ini`
* Edit the config.ini file. The provided defaults should work, but you must fill in a skype id and user name
* Run the bot, e.g. `python3 -m skypediceroller`

## Available commands
Once the bot is running and you have started a chat with it, it will respond to commands
Note: all commands start with "!" any other messages will be ignored by the bot
* `!admin`, administrator commands, you will have to add yourself to the administrators in config.ini for this command to wrok
* `!character`, character commands, manage a character's rolls
* `!example`, a well documented example command in case you want to make your own
* `!help`, this will provide an overview of available commands, and help on those commands
* `!macro`, make macros for common dice rolls
* `!name`, random name generator
* `!roll`, this will roll some dice. Examples are: `!roll 1d20+3` or `!roll 10d6`

## Making your own commands
Put a python file named after your command in the skypediceroller/commands directory.
The file should contain a class named Command that is derived from skypediceroller.core.Command.
See the example.py file for detailed documentation, or take a look at the other existing commands.