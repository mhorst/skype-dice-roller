import argparse
from html import unescape
import logging
import shlex

from skpy import SkypeMsg

from skypediceroller.core import SkypeBot


class MessageParser(argparse.ArgumentParser):
    """A class for parsing messages based on the standard python ArgumentParser"""

    def parse_message(self, skypebot: SkypeBot, message: SkypeMsg):
        """Parses a skype message"""
        # Logging
        logging.info('%s: handling "%s"' % (self.prog, message.plain))

        # Store information in class variables so it is available to sub-parsers
        MessageParser._current_skypebot = skypebot
        MessageParser._current_message = message

        # Parse the message
        try:
            result = self.parse_args(shlex.split(unescape(message.plain))[1:])
        except:
            result = None

        # Clean up the stored information
        MessageParser._current_message = None
        MessageParser._current_skypebot = None

        return result

    def reply(self, message):
        """Send a reply to the source of the message that is being parsed."""
        if (MessageParser._current_skypebot is not None) and (
            MessageParser._current_message is not None
        ):
            self._current_skypebot.reply(
                self._current_message, SkypeMsg.mono(message), rich=True
            )
        else:
            logging.error(
                "Attempting to reply while not parsing a message. Reply: '%s'" % message
            )

    def exit(self, status=0, message=None):
        """Exit function. It will send the error message as a reply, instead of exiting the entire program."""
        self.reply(message)
        raise Exception("Parse error")

    def print_help(self, file=None):
        """Print help. It will send the help as a reply, instead of printing it to std out and exiting."""
        self.reply(self.format_help())
        raise Exception("Help printed")
