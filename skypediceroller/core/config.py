import configparser


class Config:
    """The configuration of the skype dice roller bot"""

    def __init__(self, config_file):
        self._config = configparser.ConfigParser()
        self._config.read_dict(
            {
                "Skype": {
                    "user": "SKYPE USER NAME",
                    "password": "SKYPE PASSWORD",
                    "token_file": "skype.token",
                },
                "Admin": {"user_ids": ""},
                "Commands": {"enabled": "ALL", "disabled": ""},
                "Macros": {"storage_file": "", "max_uses": "10", "cooldown": "15"},
            }
        )
        self._config.read_file(config_file)

    def get_skype_user(self):
        """User name for logging into Skype"""
        return self._config["Skype"]["user"]

    def get_skype_password(self):
        """Password for logging into Skype"""
        return self._config["Skype"]["password"]

    def get_skype_token_file(self):
        """Token file to track Skype authentication"""
        return self._config["Skype"]["token_file"]

    def is_command_enabled(self, command_name: str):
        """Returns whether the command is enabled"""
        enabled = False
        disabled = False

        if self._config["Commands"]["enabled"] == "ALL":
            enabled = True
        else:
            commands = [
                command.strip()
                for command in self._config["Commands"]["enabled"].split()
            ]
            enabled = command_name in commands

        commands = [
            command.strip() for command in self._config["Commands"]["disabled"].split()
        ]
        disabled = command_name in commands

        return enabled and not disabled

    def is_admin(self, user_id: str):
        """Returns whether a given user is an admin."""
        admins = set(
            user_id.strip() for user_id in self._config["Admin"]["user_ids"].split()
        )
        return user_id in admins

    def get_macro_file_name(self):
        """Returns the file name where macros can be stored persistently"""
        return self._config["Macros"]["storage_file"]

    def get_macro_rate_limiting_settings(self):
        """Returns rate limiting configuration for macros"""
        return (
            int(self._config["Macros"]["max_uses"]),
            int(self._config["Macros"]["cooldown"]),
        )

    def get_characters_file_name(self):
        """Returns the file name where characters can be stored persistently"""
        return self._config["Characters"]["storage_file"]

    def get_groups_file_name(self):
        """Returns the file name where groups can be stored persistently"""
        return self._config["Groups"]["storage_file"]
