import importlib
import logging
import pkgutil
import sys
import os.path

import skpy

from skypediceroller.core import Config


class SkypeBot(skpy.SkypeEventLoop):
    """The Skype bot"""

    def __init__(self, config: Config) -> None:
        """Initialize the bot and connect to Skype"""
        super().__init__(
            config.get_skype_user(),
            config.get_skype_password(),
            config.get_skype_token_file(),
        )
        self._config = config
        self._running = False
        self._load_commands()

    def _load_commands(self) -> None:
        """Load available, enabled commands"""
        self._command_handlers = dict()
        self._commands = dict()
        dirname = os.path.normpath(
            os.path.join(os.path.dirname(__file__), "..", "commands")
        )
        for _, module_name, _ in pkgutil.iter_modules([dirname]):
            command_name = "!%s" % module_name
            if self._config.is_command_enabled(command_name):
                self._load_command(command_name)

    def _load_command(self, command_name: str) -> None:
        """Load a particular command"""
        import_name = "skypediceroller.commands.%s" % (command_name[1:])
        logging.info('Loading command "%s" from %s' % (command_name, import_name))
        module = importlib.import_module(import_name)
        command = module.Command(self)
        self._commands[command_name] = command
        self._command_handlers[command_name] = command
        for alias in command.get_aliases():
            self._command_handlers[alias] = command
        logging.info(
            "Registered commands: %s"
            % ", ".join([command_name] + command.get_aliases())
        )

    def is_running(self) -> bool:
        return self._running

    def get_config(self) -> Config:
        return self._config

    def get_commands(self) -> dict:
        return self._commands

    def run(self) -> None:
        """Run the bot, it will keep handling events until it is stopped"""
        logging.info("Started running")
        self._running = True
        while self._running:
            self.cycle()

    def stop(self) -> None:
        """Stop the bot, after handling any pending events"""
        self._running = False
        logging.warning("Stop requested")

    def onEvent(self, event):
        """Event handler that allows the bot to respond to messages"""
        logging.debug(event)
        if isinstance(event, skpy.SkypeNewMessageEvent) or isinstance(
            event, skpy.SkypeEditMessageEvent
        ):
            if isinstance(event.msg, skpy.msg.SkypeTextMsg):
                if (event.msg.content is not None) and event.msg.plain.startswith("!"):
                    logging.info(
                        'Command from user "%s" in "%s" : "%s"'
                        % (event.msg.user.id, event.msg.chat.id, event.msg.content)
                    )
                    command_name = event.msg.plain.split()[0]
                    if command_name in self._command_handlers:
                        self._command_handlers[command_name].handle_message(event.msg)

    def reply(self, message: skpy.SkypeMsg, reply: str, rich=False) -> None:
        """Sends a reply to the given message"""
        logging.info(
            'Reply to user "%s" in "%s" : "%s"'
            % (message.user.id, message.chat.id, reply)
        )
        message.chat.sendMsg(reply, rich=rich)
