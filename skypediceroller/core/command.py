from abc import abstractmethod

from skpy import SkypeMsg

from skypediceroller.core import SkypeBot, MessageParser, Singleton


class Command(metaclass=Singleton):
    def __init__(self, skypebot: SkypeBot):
        self.skypebot = skypebot
        self.parser = self.get_parser()

    def get_parser(self):
        """Returns a parser for messages that are sent to this command."""
        prog = "!%s" % type(self).__module__.split(".")[-1]
        epilog = None
        if self.get_aliases():
            epilog = "Aliases : %s" % ", ".join([prog] + self.get_aliases())
        parser = MessageParser(
            prog=prog,
            description=self.get_description(),
            epilog=epilog,
        )
        return parser

    def get_aliases(self):
        """Returns a list of aliases for this command"""
        return []

    @abstractmethod
    def get_description(self):
        """Returns a description of this command"""

    def handle_message(self, message: SkypeMsg):
        """Handle an incoming message"""
        args = self.parser.parse_message(self.skypebot, message)
        if args is not None:
            self.handle_command(message, args)

    @abstractmethod
    def handle_command(self, message: SkypeMsg, args):
        """Handle a command with parsed arguments"""
