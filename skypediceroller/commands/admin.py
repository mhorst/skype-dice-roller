import skpy

import skypediceroller.core


class Command(skypediceroller.core.Command):
    """Administration commands"""

    def get_description(self):
        return "Admin commands"

    def get_parser(self):
        parser = super().get_parser()
        subparsers = parser.add_subparsers(
            title="subcommands", dest="subcommand", required=True
        )

        # Requests
        parser_requests = subparsers.add_parser(
            "requests", help="Administer contact requests"
        )
        parser_requests.set_defaults(func=self.handle_requests)
        parser_requests.add_argument(
            "--accept",
            type=str,
            metavar="USER_ID",
            help="Accept a connection request from the specified user id",
        )
        parser_requests.add_argument(
            "--decline",
            type=str,
            metavar="USER_ID",
            help="Decline a connection request from the specified user id",
        )

        # Quit command
        parser_quit = subparsers.add_parser("quit", help="Stop the bot")
        parser_quit.set_defaults(func=self.handle_quit)

        return parser

    def check_message_is_from_admin(self, message: skpy.SkypeMsg):
        """Returns whether the message is from an admin or not"""
        result = self.skypebot.get_config().is_admin(message.user.id)
        if not result:
            self.skypebot.reply(message, 'User "%s" is not an admin.' % message.user.id)
        return result

    def handle_command(self, message: skpy.SkypeMsg, args):
        """Handle the command"""
        # Only allow admins access
        if not self.check_message_is_from_admin(message):
            return

        # Forward the message to the appropriate function
        if "func" in args:
            args.func(message, args)

    def handle_quit(self, message: skpy.SkypeMsg, args):
        """Handle the quit command"""
        self.skypebot.reply(message, "The bot will now quit.")
        self.skypebot.stop()

    def handle_requests(self, message: skpy.SkypeMsg, args):
        """Handle the requests command"""
        request_list = self.skypebot.contacts.requests()
        answer = []
        accept = []
        decline = []

        for request in request_list:
            answer.append(
                '%s "%s" %s %s'
                % (request.time, request.user.id, request.user.name, request.greeting)
            )
            if request.user.id == args.accept:
                accept.append(request)
            if request.user.id == args.decline:
                decline.append(request)
        answer.append("%d pending contact requests" % len(request_list))

        if (args.accept is not None) and len(accept) == 0:
            answer.append(
                'No connection request with user id "%s" found.' % args.accept
            )
        else:
            for request in accept:
                request.accept()
                answer.append('Accepting request from user id "%s".' % args.accept)

        if (args.decline is not None) and len(decline) == 0:
            answer.append(
                'No connection request with user id "%s" found.' % args.accept
            )
        else:
            for request in decline:
                request.decline()
                answer.append('Declining request from user id "%s".' % args.decline)

        self.skypebot.reply(message, "\n".join(answer))
