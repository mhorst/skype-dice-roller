import skpy
import skypediceroller.core
import skypediceroller.commands.roll

import argparse
import json
import os


class Character:
    def __init__(self, name, data={}):
        self.name = name.strip()
        self.data = data

    def is_name_matching_with(self, name):
        """Checks whether the character's name matches with the provided name"""
        return name.strip().lower() == self.name.lower()

    def get_possessive_name(self):
        """Returns the possesive variant of the character's name"""
        if self.name.lower()[-1] == "s":
            return "%s'" % self.name
        else:
            return "%s's" % self.name

    def find_check(self, check, partial_match=True):
        """Find a particular check on the character sheet"""
        check = check.strip()
        if check in self.data:
            return (check, self.data[check])
        else:
            # Look for the check with some fuzziness
            # Ignore case
            check = check.lower()
            for (entry, roll) in self.data.items():
                if check == entry.lower():
                    return (entry, roll)
            # Partial match
            if partial_match:
                for (entry, roll) in self.data.items():
                    if entry.lower().startswith(check):
                        return (entry, roll)

        return (None, None)


class Command(skypediceroller.core.Command):
    """Character command, allowing managing of characters and rolling for them"""

    def __init__(self, skypebot: skypediceroller.core.SkypeBot):
        super().__init__(skypebot)
        self._roller = skypediceroller.commands.roll.Command(skypebot)

        # Retrieve characters from disc
        self._characters_file_name = (
            self.skypebot.get_config().get_characters_file_name()
        )
        if (self._characters_file_name) and (
            os.path.isfile(self._characters_file_name)
        ):
            self._load_data()
        else:
            self._characters = dict()
            self._defaults = dict()

    def _load_data(self):
        with open(self._characters_file_name, "r") as infile:
            data = json.load(infile)
            self._characters = {
                name: Character(name, character_data)
                for name, character_data in data["characters"].items()
            }
            self._defaults = data["defaults"]

    def _save_data(self):
        with open(self._characters_file_name, "w") as outfile:
            characters = {
                name: self._characters[name].data for name in self._characters
            }
            data = {"characters": characters, "defaults": self._defaults}
            json.dump(data, outfile, indent=2, sort_keys=True)

    def get_parser(self):
        """Returns a parser for messages that are sent to this command."""
        parser = super().get_parser()
        subparsers = parser.add_subparsers()

        # Common options
        common_parser = argparse.ArgumentParser(add_help=False)
        common_parser.add_argument(
            "--name",
            help="Name of the character (in case you do not want to use your default character)",
        )

        # Create command
        subparser = subparsers.add_parser(
            "create", help="Create a new character (and set as default)"
        )
        subparser.add_argument("name", help="Name of the new character")
        subparser.set_defaults(func=self.handle_subcommand_create)

        # List command
        subparser = subparsers.add_parser(
            "list", aliases=["l"], help="List all characters"
        )
        subparser.set_defaults(func=self.handle_subcommand_list)

        # Default command
        subparser = subparsers.add_parser(
            "default",
            help="Set a character as the default character for you",
        )
        subparser.add_argument("name", nargs="?", help="Name of your default character")
        subparser.set_defaults(func=self.handle_subcommand_default)

        # Set command
        subparser = subparsers.add_parser(
            "set",
            parents=[common_parser],
            help="Update a character sheet to set a check to a specific roll",
        )
        subparser.add_argument("check", help="Name of the check to set")
        subparser.add_argument(
            "roll", nargs="?", help='The roll expression, e.g. "1d20+3"'
        )
        subparser.set_defaults(func=self.handle_subcommand_set)

        # Show command
        subparser = subparsers.add_parser(
            "show", parents=[common_parser], help="Show a character sheet"
        )
        subparser.add_argument(
            "check", nargs="?", help="Show only this check from the character sheet"
        )
        subparser.set_defaults(func=self.handle_subcommand_show)

        # Roll command
        subparser = subparsers.add_parser(
            "roll", aliases=["r"], parents=[common_parser], help="Roll a check"
        )
        subparser.add_argument("check", help="Which check to roll")
        subparser.set_defaults(func=self.handle_subcommand_roll)

        return parser

    def get_aliases(self):
        """Returns a list of aliases for this command"""
        return ["!c", "!char"]

    def get_description(self):
        """Returns a description of this command"""
        return "Character management command"

    def handle_command(self, message: skpy.SkypeMsg, args):
        """Handle a command with parsed arguments"""
        # Send to sub-command handler
        if hasattr(args, "func"):
            args.func(message, args)

    def _find_character_by_name(self, name: str):
        """Find a character in the database based on the name"""
        if name in self._characters:
            # Look for the name
            return self._characters[name]
        else:
            # Look for the name with some fuzziness
            for character in self._characters.values():
                if character.is_name_matching_with(name):
                    return character

        # Character not found
        return None

    def _find_character(self, message: skpy.SkypeMsg, args):
        """Find a character based on arguments and who is asking.
        This function will reply with appropriate error messages if the character is not found."""
        character = None

        if args.name is not None:
            # Command-line argument was used to specify the character
            name = args.name.strip()
            character = self._find_character_by_name(name)
            if character is None:
                self.skypebot.reply(
                    message,
                    'A character named "%s" does not exist.' % (args.name.strip()),
                )
        else:
            # No command-line character, we have to rely on the default character for this user
            if message.user.id not in self._defaults:
                self.skypebot.reply(
                    message,
                    "%s currently has no default character." % (message.user.name),
                )
            else:
                character = self._find_character_by_name(
                    self._defaults[message.user.id]
                )
                if character is None:
                    self.skypebot.reply(
                        message,
                        'The default character for %s is "%s", but this character is not in the database.'
                        % (message.user.name, self._defaults[message.user.id]),
                    )

        return character

    def handle_subcommand_create(self, message: skpy.SkypeMsg, args):
        name = args.name.strip()
        character = self._find_character_by_name(name)
        if character is not None:
            self.skypebot.reply(
                message, 'A character named "%s" already exists.' % character.name
            )
        else:
            self._characters[name] = Character(name)
            self._defaults[message.user.id] = name
            self._save_data()
            self.skypebot.reply(
                message,
                'Created a new character named "%s", and set it as the default character for %s'
                % (name, message.user.name),
            )

    def handle_subcommand_list(self, message: skpy.SkypeMsg, args):
        if len(self._characters) > 0:
            self.skypebot.reply(
                message,
                "The following characters are in the database: %s"
                % (", ".join(name for name in self._characters)),
            )
        else:
            self.skypebot.reply(message, "There are no characters in the database")

    def handle_subcommand_default(self, message: skpy.SkypeMsg, args):
        if args.name is None:
            # User is just asking for the current default
            if message.user.id in self._defaults:
                self.skypebot.reply(
                    message,
                    'The default character for %s is "%s".'
                    % (message.user.name, self._defaults[message.user.id]),
                )
            else:
                self.skypebot.reply(
                    message,
                    "%s currently has no default character." % (message.user.name),
                )
        else:
            # User is setting the default
            character = self._find_character_by_name(args.name.strip())
            if character is not None:
                self._defaults[message.user.id] = character.name
                self._save_data()
                self.skypebot.reply(
                    message,
                    'The default character for %s will be "%s" from now on.'
                    % (message.user.name, self._defaults[message.user.id]),
                )
            else:
                self.skypebot.reply(
                    message,
                    'A character named "%s" does not exist, so the default for %s is not updated.'
                    % (args.name.strip(), message.user.name),
                )

    def show_check(self, message: skpy.SkypeMsg, args):
        character = self._find_character(message, args)
        if character is not None:
            (check, roll) = character.find_check(args.check)
            if check is None:
                self.skypebot.reply(
                    message,
                    "%s does not have an entry for %s." % (character.name, args.check),
                )
            else:
                self.skypebot.reply(
                    message,
                    "%s %s = %s." % (character.get_possessive_name(), check, roll),
                )

    def handle_subcommand_set(self, message: skpy.SkypeMsg, args):
        if args.roll is None:
            # Show the check
            self.show_check(message, args)
        else:
            character = self._find_character(message, args)
            if character is not None:
                # Set the check
                (check, _) = character.find_check(args.check, partial_match=False)
                if check is None:
                    check = args.check.strip()
                character.data[check] = args.roll.strip()
                self._save_data()
                self.show_check(message, args)

    def handle_subcommand_show(self, message: skpy.SkypeMsg, args):
        if args.check is not None:
            self.show_check(message, args)
        else:
            character = self._find_character(message, args)
            if character is not None:
                sheet = skpy.SkypeMsg.bold(character.name) + "\n"
                sheet += skpy.SkypeMsg.mono(
                    "\n".join(
                        [
                            "%s%s." % (check.ljust(20, "."), character.data[check])
                            for check in sorted(character.data)
                        ]
                    )
                )
                self.skypebot.reply(message, sheet, rich=True)

    def handle_subcommand_roll(self, message: skpy.SkypeMsg, args):
        character = self._find_character(message, args)
        if character is not None:
            (check, roll) = character.find_check(args.check)
            if roll is None:
                self.skypebot.reply(
                    message,
                    "%s does not have an entry for %s." % (character.name, args.check),
                )
            else:
                self.skypebot.reply(
                    message,
                    "%s %s :\n" % (character.get_possessive_name(), check)
                    + self._roller.dice_rolls(roll),
                    rich=True,
                )
