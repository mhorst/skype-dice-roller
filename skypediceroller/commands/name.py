import names
import skpy

import skypediceroller.core


class Command(skypediceroller.core.Command):
    """Random name generator"""

    def get_description(self):
        return "Generates a random name"

    def handle_command(self, message: skpy.SkypeMsg, args):
        """Handle the command"""
        self.skypebot.reply(message, names.get_full_name())
