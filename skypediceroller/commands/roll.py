import argparse
import random

from lark import Lark, Transformer, UnexpectedInput, v_args
import skpy

import skypediceroller.core


dice_roll_grammar = """
    ?start: sum
    ?sum: product
        | sum "+" product   -> add
        | sum "-" product   -> sub
    ?product: roll
        | product "*" roll  -> mul
        | product "/" roll  -> div
    ?roll: atom
        | roll "d" atom     -> roll
        | roll "D" atom     -> roll
    ?atom: NUMBER           -> number
         | "-" atom         -> neg
         | "(" sum ")"      -> brackets
    %import common.NUMBER
    %import common.WS_INLINE
    %ignore WS_INLINE
"""


@v_args(inline=True)
class RollDice(Transformer):
    def add(self, left, right):
        return "%s+%s" % (left[0], right[0]), left[1] + right[1]

    def sub(self, left, right):
        return "%s-%s" % (left[0], right[0]), left[1] - right[1]

    def mul(self, left, right):
        return "%s*%s" % (left[0], right[0]), left[1] * right[1]

    def div(self, left, right):
        return "%s/%s" % (left[0], right[0]), left[1] / right[1]

    def _format_dice_roll(self, dice, roll):
        result = "%d" % roll
        if roll == 1:
            result = skpy.SkypeMsg.bold(result)
        if roll == dice:
            result = skpy.SkypeMsg.bold(result)
        return result

    def roll(self, number, dice):
        number = number[1]
        dice = dice[1]
        rolls = [random.randint(1, dice) for i in range(number)]
        value = sum(rolls)
        description = ("(%s)" if (number > 1) else "%s") % "+".join(
            self._format_dice_roll(dice, roll) for roll in rolls
        )

        return description, value

    def number(self, value):
        v = float(value)
        try:
            v = int(value)
        except ValueError:
            pass
        return str(v), v

    def neg(self, value):
        return "-%s" % value[0], -value[1]

    def brackets(self, value):
        return "(%s)" % value[0], value[1]


class Command(skypediceroller.core.Command):
    def __init__(self, skypebot: skypediceroller.core.SkypeBot):
        super().__init__(skypebot)
        self.expression_parser = Lark(
            dice_roll_grammar, parser="lalr", transformer=RollDice()
        )

    def get_aliases(self):
        return ["!r"]

    def get_description(self):
        return "Roll some dice"

    def get_parser(self):
        parser = super().get_parser()
        parser.add_argument(
            "--description",
            "--descr",
            "-d",
            help="Description of the dice roll",
        )
        parser.add_argument(
            "dice",
            nargs=argparse.REMAINDER,
            help="The dice to roll, e.g. 1d20, 1d8+3, or 10d6. Use a comma to separate multiple dice rolls.",
        )
        return parser

    def dice_roll(self, expression):
        """Returns the dice roll based on the provided expression"""
        try:
            description, value = self.expression_parser.parse(expression)
            return "%s = %g" % (description, value), value
        except UnexpectedInput as e:
            return (
                "Error parsing expression:\n%s"
                % skpy.SkypeMsg.mono(e.get_context(expression)),
                None,
            )

    def dice_rolls(self, expression):
        """Returns the dice rolls based on the provided expressions,
        which can contain multiple dice rolls seperated by a comma"""
        expressions = expression.split(",")
        results = [self.dice_roll(expression)[0] for expression in expressions]
        if len(results) == 1:
            description = "%s = %s" % (expressions[0], results[0])
        else:
            description = [
                ("#%d : %s = %s\n" % ((idx + 1), expressions[idx], result))
                for (idx, result) in enumerate(results)
            ]
            description = "".join(description)

        return description

    def handle_command(self, message: skpy.SkypeMsg, args):
        """Handle the command"""
        expression = "".join(args.dice)

        if args.description is not None:
            header = "%s :\n" % (args.description)
        else:
            header = ""

        roll = self.dice_rolls(expression)

        self.skypebot.reply(message, header + roll, rich=True)


if __name__ == "__main__":
    import sys

    expression = "".join(sys.argv[1:])

    cmd = Command(None)
    print(cmd.dice_rolls(expression))
