from ast import literal_eval
import datetime
import json
import os.path

import skpy

import skypediceroller.core


class Command(skypediceroller.core.Command):
    """Macro command, allowing users to specify macros"""

    def __init__(self, skypebot: skypediceroller.core.SkypeBot):
        super().__init__(skypebot)

        # Get rate limiting settings
        (
            self._maximum_invocations,
            self._cooldown,
        ) = self.skypebot.get_config().get_macro_rate_limiting_settings()
        self._cooldown = datetime.timedelta(seconds=self._cooldown)
        self._uses = []

        # Retrieve macros from disc
        self._macro_file_name = self.skypebot.get_config().get_macro_file_name()
        if (self._macro_file_name) and (os.path.isfile(self._macro_file_name)):
            self._load_macros()
        else:
            self._macros = dict()

    def _load_macros(self):
        with open(self._macro_file_name, "r") as infile:
            data = json.load(infile)
            self._macros = {
                literal_eval(key): definition for key, definition in data.items()
            }

    def _save_macros(self):
        with open(self._macro_file_name, "w") as outfile:
            json.dump(
                {str(key): definition for (key, definition) in self._macros.items()},
                outfile,
                indent=2,
            )

    def get_aliases(self):
        return ["!m", "!run"]

    def get_description(self):
        return "Run and define macros"

    def get_parser(self):
        parser = super().get_parser()

        context_group = parser.add_mutually_exclusive_group()
        context_group.add_argument(
            "--global",
            dest="context",
            action="store_const",
            const="global",
            help="Global macro, available for all users in all chats",
        )
        context_group.add_argument(
            "--user",
            dest="context",
            action="store_const",
            const="user",
            help="User macro, only available for the user who created it",
        )
        context_group.add_argument(
            "--chat",
            dest="context",
            action="store_const",
            const="chat",
            help="Chat macro, available for all users in the current chat",
        )
        context_group.add_argument(
            "--chat-user",
            dest="context",
            action="store_const",
            const="chat-user",
            help="Macro for the current chat/user combination",
        )

        group = parser.add_mutually_exclusive_group()
        group.add_argument(
            "macro",
            nargs="?",
            metavar="MACRO",
            type=str,
            help="The name of the macro to run",
        )
        group.add_argument(
            "--set",
            nargs=2,
            metavar=("MACRO", "CONTENTS"),
            help="Define the contents of the macro",
        )
        group.add_argument(
            "--show",
            action="store_true",
            help="Show the available macros",
        )

        parser.epilog = """When running a macro, unless a context is explicitly specified, the command will search for the macro in chat-user, chat, user, global context respectively.\n
        When setting a macro value, unless a context is explicitly specified, the macro will be set for the chat/user combination."""
        return parser

    def get_macro_key(self, message: skpy.SkypeMsg, context, macro):
        """Returns the key for the specified macro and a human readable specification"""
        if context not in ["global", "user", "chat", "chat-user"]:
            context = "chat-user"

        chat_description = (
            message.chat.topic
            if isinstance(message.chat, skpy.SkypeGroupChat)
            else message.chat.id
        )

        if context == "global":
            key = (None, None, macro)
            description = "for everyone"
        elif context == "user":
            key = (None, message.user.id, macro)
            description = 'for user "%s"' % message.user.name
        elif context == "chat":
            key = (message.chat.id, None, macro)
            description = 'for chat "%s"' % chat_description
        else:
            key = (message.chat.id, message.user.id, macro)
            description = 'for user "%s" in chat "%s"' % (
                message.user.name,
                chat_description,
            )

        return (key, description)

    def find_macro(self, message, context, macro):
        """Looks for the macro in (possibly multiple) context and returns the key,
        or None if the macro could not be found."""
        key = None

        if context is not None:
            # The user specified a context explicitly
            key, _ = self.get_macro_key(message, context, macro)
            if key not in self._macros:
                key = None
        else:
            # No context specified, look through all of them
            for context in ["global", "user", "chat", "chat-user"]:
                temp_key, _ = self.get_macro_key(message, context, macro)
                if temp_key in self._macros:
                    key = temp_key
        return key

    def reply_macro_list(self, message: skpy.SkypeMsg, header, macro_list):
        """Sends a reply with macro definitions the user,
        the header is sent first, and the macro_list should be a list of tuples,
        consisting of the macro name and the macro definiton"""
        answer = [header]
        for (name, definition) in sorted(macro_list):
            answer.append("%s : %s" % (name, definition))
        answer.append("%d macros defined" % len(macro_list))
        self.skypebot.reply(message, "\n".join(answer))

    def show_macros(self, message: skpy.SkypeMsg, context):
        """Shows the available macros"""

        if (context is None) or (context == "global"):
            self.reply_macro_list(
                message,
                "Global macros:",
                [
                    (name, definition)
                    for ((chat, user, name), definition) in self._macros.items()
                    if (chat == None) and (user == None)
                ],
            )
        if (context is None) or (context == "user"):
            self.reply_macro_list(
                message,
                "Macros for %s:" % message.user.name,
                [
                    (name, definition)
                    for ((chat, user, name), definition) in self._macros.items()
                    if (chat == None) and (user == message.user.id)
                ],
            )
        if (context is None) or (context == "chat"):
            self.reply_macro_list(
                message,
                "Macros for this chat:",
                [
                    (name, definition)
                    for ((chat, user, name), definition) in self._macros.items()
                    if (chat == message.chat.id) and (user == None)
                ],
            )
        if (context is None) or (context == "chat-user"):
            self.reply_macro_list(
                message,
                "Macros for %s in this chat:" % message.user.name,
                [
                    (name, definition)
                    for ((chat, user, name), definition) in self._macros.items()
                    if (chat == message.chat.id) and (user == message.user.id)
                ],
            )

    def handle_command(self, message: skpy.SkypeMsg, args):
        """Handle the command"""
        if args.set is not None:
            # Define the macro
            key, description = self.get_macro_key(message, args.context, args.set[0])
            self._macros[key] = args.set[1]
            if self._macro_file_name:
                self._save_macros()
            self.skypebot.reply(
                message,
                'Macro %s has been updated to "%s" %s'
                % (args.set[0], args.set[1], description),
            )
        elif args.show:
            # Show the macros
            self.show_macros(message, args.context)
        else:
            # Run the macro
            key = self.find_macro(message, args.context, args.macro)
            if key is not None:
                now = datetime.datetime.now()
                print(self._uses)
                self._uses = [
                    use for use in self._uses if use >= (now - self._cooldown)
                ]
                if len(self._uses) < self._maximum_invocations:
                    self._uses.append(now)
                    self.skypebot.reply(message, self._macros[key])
                else:
                    self.skypebot.reply(
                        message,
                        'Sorry, not executing "%s" due to rate limiting' % args.macro,
                    )
            else:
                self.skypebot.reply(message, 'Unknown macro "%s"' % args.macro)
