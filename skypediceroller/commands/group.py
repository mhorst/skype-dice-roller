import skpy
import skypediceroller.core
import skypediceroller.commands.roll

import argparse
import json
import os


class Group:
    def __init__(self, name, members=[]):
        self.name = name.strip()
        self.members = members

    def is_name_matching_with(self, name):
        """Checks whether the group's name matches with the provided name"""
        return name.strip().lower() == self.name.lower()


class Command(skypediceroller.core.Command):
    """Group command, allowing managing of groups of characters and rolling for them"""

    def __init__(self, skypebot: skypediceroller.core.SkypeBot):
        super().__init__(skypebot)
        self._character_db = skypediceroller.commands.character.Command(skypebot)
        self._roller = skypediceroller.commands.roll.Command(skypebot)

        # Retrieve groups from disc
        self._groups_file_name = self.skypebot.get_config().get_groups_file_name()
        if (self._groups_file_name) and (os.path.isfile(self._groups_file_name)):
            self._load_data()
        else:
            self._groups = dict()
            self._default = None

    def _load_data(self):
        with open(self._groups_file_name, "r") as infile:
            data = json.load(infile)
            self._groups = data["groups"]
            self._groups = {
                name: Group(name, members) for name, members in data["groups"].items()
            }
            self._default = data["default"]

    def _save_data(self):
        with open(self._groups_file_name, "w") as outfile:
            groups = {name: self._groups[name].members for name in self._groups}
            data = {"groups": groups, "default": self._default}
            json.dump(data, outfile, indent=2, sort_keys=True)

    def get_parser(self):
        """Returns a parser for messages that are sent to this command."""
        parser = super().get_parser()
        subparsers = parser.add_subparsers()

        # Common options
        common_parser = argparse.ArgumentParser(add_help=False)
        common_parser.add_argument(
            "--name",
            help="Name of the group (in case you do not want to use the default group)",
        )

        # Create command
        subparser = subparsers.add_parser(
            "create", help="Create a new group (and set as default)"
        )
        subparser.add_argument("name", help="Name of the new group")
        subparser.set_defaults(func=self.handle_subcommand_create)

        # List command
        subparser = subparsers.add_parser("list", aliases=["l"], help="List all groups")
        subparser.set_defaults(func=self.handle_subcommand_list)

        # Default command
        subparser = subparsers.add_parser(
            "default",
            help="Set a group  as the default group",
        )
        subparser.add_argument("name", nargs="?", help="Name of the default group")
        subparser.set_defaults(func=self.handle_subcommand_default)

        # Add command
        subparser = subparsers.add_parser(
            "add",
            parents=[common_parser],
            help="Add one or more characters to a group",
        )
        subparser.add_argument(
            "character", nargs="+", help="The name of the character(s)"
        )
        subparser.set_defaults(func=self.handle_subcommand_add)

        # Remove command
        subparser = subparsers.add_parser(
            "remove",
            parents=[common_parser],
            help="Remove one or more characters from a group",
        )
        subparser.add_argument(
            "character", nargs="+", help="The name of the character(s)"
        )
        subparser.set_defaults(func=self.handle_subcommand_remove)

        # Show command
        subparser = subparsers.add_parser(
            "show", parents=[common_parser], help="Show a group"
        )
        subparser.set_defaults(func=self.handle_subcommand_show)

        # Roll command
        subparser = subparsers.add_parser(
            "roll", aliases=["r"], parents=[common_parser], help="Roll a group check"
        )
        subparser.add_argument("check", help="Which check to roll")
        subparser.set_defaults(func=self.handle_subcommand_roll)

        return parser

    def get_aliases(self):
        """Returns a list of aliases for this command"""
        return ["!g"]

    def get_description(self):
        """Returns a description of this command"""
        return "Group management command"

    def handle_command(self, message: skpy.SkypeMsg, args):
        """Handle a command with parsed arguments"""
        # Send to sub-command handler
        if hasattr(args, "func"):
            args.func(message, args)

    def _find_group_by_name(self, name: str):
        """Find a group in the database based on the name"""
        if name in self._groups:
            # Look for the name
            return self._groups[name]
        else:
            # Look for the name with some fuzziness
            for group in self._groups.values():
                if group.is_name_matching_with(name):
                    return group

        # Group not found
        return None

    def _find_group(self, message: skpy.SkypeMsg, args):
        """Find a group based on arguments and defaults.
        This function will reply with appropriate error messages if the group is not found."""
        group = None

        if args.name is not None:
            # Command-line argument was used to specify the group
            name = args.name.strip()
            group = self._find_group_by_name(name)
            if group is None:
                self.skypebot.reply(
                    message,
                    'A group named "%s" does not exist.' % (args.name.strip()),
                )
        else:
            # No command-line group, we have to rely on the default group
            if self._default is None:
                self.skypebot.reply(
                    message,
                    "Currently there is no default group.",
                )
            else:
                group = self._find_group_by_name(self._default)
                if group is None:
                    self.skypebot.reply(
                        message,
                        'The default group is "%s", but this group is not in the database.'
                        % (self._default),
                    )

        return group

    def handle_subcommand_create(self, message: skpy.SkypeMsg, args):
        name = args.name.strip()
        group = self._find_group_by_name(name)
        if group is not None:
            self.skypebot.reply(
                message, 'A group named "%s" already exists.' % group.name
            )
        else:
            self._groups[name] = Group(name)
            self._default = name
            self._save_data()
            self.skypebot.reply(
                message,
                'Created a new group named "%s", and set it as the default' % (name),
            )

    def handle_subcommand_list(self, message: skpy.SkypeMsg, args):
        if len(self._groups) > 0:
            self.skypebot.reply(
                message,
                "The following groups are in the database: %s"
                % (", ".join(name for name in self._groups)),
            )
        else:
            self.skypebot.reply(message, "There are no groups in the database")

    def handle_subcommand_default(self, message: skpy.SkypeMsg, args):
        if args.name is None:
            # User is just asking for the current default
            if self._default is not None:
                self.skypebot.reply(
                    message,
                    'The default group is "%s".' % (self._default),
                )
            else:
                self.skypebot.reply(
                    message,
                    "Currently there is not default group.",
                )
        else:
            # User is setting the default
            group = self._find_group_by_name(args.name.strip())
            if group is not None:
                self._default = group.name
                self._save_data()
                self.skypebot.reply(
                    message,
                    'The default group will be "%s" from now on.' % (self._default),
                )
            else:
                self.skypebot.reply(
                    message,
                    'A group named "%s" does not exist, so the default is not updated.'
                    % (args.name.strip()),
                )

    def show_group(self, message: skpy.SkypeMsg, args):
        group = self._find_group(message, args)
        if group is not None:
            if len(group.members) > 0:
                self.skypebot.reply(
                    message,
                    'The group "%s" consists of: %s'
                    % (group.name, ", ".join(group.members)),
                )
            else:
                self.skypebot.reply(
                    message,
                    'The group "%s" currently has no characters in it' % (group.name),
                )

    def handle_subcommand_add(self, message: skpy.SkypeMsg, args):
        group = self._find_group(message, args)
        if group is not None:
            # Find the characters
            for character_name in args.character:
                character = self._character_db._find_character_by_name(character_name)
                if character is not None:
                    if character.name not in group.members:
                        group.members.append(character.name)
                else:
                    self.skypebot.reply(
                        message,
                        'The character "%s" is not known' % (character_name),
                    )

            self._save_data()
            self.show_group(message, args)

    def handle_subcommand_remove(self, message: skpy.SkypeMsg, args):
        group = self._find_group(message, args)
        if group is not None:
            # Remove the characters
            for character_name in args.character:
                character = self._character_db._find_character_by_name(character_name)
                if character.name in group.members:
                    group.members.remove(character.name)
            self._save_data()
            self.show_group(message, args)

    def handle_subcommand_show(self, message: skpy.SkypeMsg, args):
        self.show_group(message, args)

    def handle_subcommand_roll(self, message: skpy.SkypeMsg, args):
        group = self._find_group(message, args)
        if group is not None:
            results = []
            errors = []
            for character_name in group.members:
                character = self._character_db._find_character_by_name(character_name)
                if character is None:
                    errors.append(
                        'Could not find a character named "%s"' % character_name
                    )
                else:
                    (check, roll) = character.find_check(args.check)
                    if roll is None:
                        errors.append(
                            '%s does not have an entry for "%s".'
                            % (character.name, args.check)
                        )
                    else:
                        description, value = self._roller.dice_roll(roll)
                        result = (
                            value,
                            "%s %s : %s = "
                            % (character.get_possessive_name(), check, roll)
                            + description,
                        )
                        results.append(result)
            results.sort(reverse=True)

            reply = "\n".join(roll for (value, roll) in results)
            if len(errors) > 0:
                reply += "\n\nNote:\n" + "\n".join(errors)
            self.skypebot.reply(message, reply, rich=True)
