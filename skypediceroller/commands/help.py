import skpy

import skypediceroller.core


class Command(skypediceroller.core.Command):
    def get_aliases(self):
        """Returns a list of aliases for this command"""
        return ["!h", "!?"]

    def get_description(self):
        return "Provides help on the available commands"

    def get_parser(self):
        parser = super().get_parser()
        parser.add_argument(
            "command",
            nargs="?",
            help="The command on which you want help",
        )
        return parser

    def handle_command(self, message: skpy.SkypeMsg, args):
        """Handle the command"""
        if args.command is None:
            self.reply_with_general_help(message)
        else:
            self.reply_with_help_for_command(message, args.command)

    def reply_with_general_help(self, message: skpy.SkypeMsg):
        answer = []
        answer.append(
            "Use !help <command> with any of the following commands to get more detailed help."
        )
        commands = self.skypebot.get_commands()
        for (name, command_instance) in commands.items():
            aliases = command_instance.get_aliases()
            if len(aliases) == 0:
                answer.append("%s - %s" % (name, command_instance.get_description()))
            else:
                answer.append(
                    "%s (a.k.a. %s) - %s"
                    % (name, ", ".join(aliases), command_instance.get_description())
                )

        self.skypebot.reply(message, "\n".join(answer))

    def find_command(self, command_name):
        command_name = "!%s" % command_name.strip("!")
        commands = self.skypebot.get_commands()
        command = None
        if command_name in commands:
            command = commands[command_name]
        else:
            # Look for aliases
            for command_instance in commands.values():
                if command_name in command_instance.get_aliases():
                    command = command_instance

        return command

    def reply_with_help_for_command(self, message: skpy.SkypeMsg, command_name):
        # Find the command
        command = self.find_command(command_name)
        if command is None:
            self.skypebot.reply(message, "Command %s is unknown" % command_name)
        else:
            self.skypebot.reply(message, command.parser.format_help())