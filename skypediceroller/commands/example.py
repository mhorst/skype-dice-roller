# This is an example of a command for the skype dice roller bot,
# If you want to make your own command, you can follow this file as an example

# Note that this file is called example.py
# If you want to make !yourcommand, make a file yourcommand.py in this same directory
# The bot will automatically load it and make it available.

# Some imports will be necessary:
import skpy
import skypediceroller.core


# Your file needs to make a class named Command that derives from skypediceroller.core.Command:
class Command(skypediceroller.core.Command):
    """Example command for educational purposes"""

    # You can override the constructor, if you want to initialize some fields in your class
    # just ensure that you still call the constructor of the superclass
    # def __init__(self, skypebot: skypediceroller.core.SkypeBot):
    #    super().__init__(skypebot)

    # If you want your command to accept parameters, or options, use this function
    # use the superclass to get a basic parser, and then you can add arguments to it.
    # Ensure that your function returns the parser
    # See the Python documentation for "argparse" for the available options.
    # def get_parser(self):
    #    """Returns a parser for messages that are sent to this command."""
    #    parser = super().get_parser()
    #    #parser.add_argument("my_argument")
    #    return parser

    # If your command has any other ways of being invoked, e.g. abbreviations or aliases,
    # then this function should return a list of those
    # If you don't have any aliases you can leave this function out
    def get_aliases(self):
        """Returns a list of aliases for this command"""
        return ["!ex", "!eg"]

    # This function should return a short description for your command
    # It will automatically be added to the !help command, and to the
    # -h option of your command
    # This is one of the few functions that you must implement
    def get_description(self):
        """Returns a description of this command"""
        return "An example command"

    # This function actually handles an incoming command
    # The message parameter contains the raw skype message as received by the bot
    # The args parameter contains the message parsed by the parser from your get_parser function
    # This function is only called if the parsing succeeded, and the user did not use the -h option
    def handle_command(self, message: skpy.SkypeMsg, args):
        """Handle a command with parsed arguments"""
        # To send a simple reply to the command you can use the following:
        self.skypebot.reply(message, "Hello World!")

        # If you want to send some fancy formatted message use
        # skpy.SkypeMsg functions for romatting,
        # and set rich=True when calling the reply function:
        self.skypebot.reply(
            message,
            skpy.SkypeMsg.bold("This ")
            + skpy.SkypeMsg.italic("is ")
            + skpy.SkypeMsg.mono("an ")
            + skpy.SkypeMsg.colour("example! ", "#FF0000")
            + skpy.SkypeMsg.emote("(nerdy)")
            + skpy.SkypeMsg.emote(":)"),
            rich=True,
        )
        # See the skpy API reference for all options
        # Unfortunately the API for coloring text does not seem to be operational
