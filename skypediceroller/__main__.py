import argparse
import logging
import signal

from skypediceroller.core import Config
from skypediceroller.core import SkypeBot

if __name__ == "__main__":
    # Command-line argument parsing
    parser = argparse.ArgumentParser(
        prog="skypediceroller",
        description="Skype Dice Roller",
    )
    parser.add_argument(
        "--config",
        type=argparse.FileType("r"),
        default="config.ini",
        help="Name of the configuration file (default: config.ini)",
    )
    parser.add_argument(
        "-v",
        dest="log_level",
        action="store_const",
        const=logging.INFO,
        default=logging.WARNING,
        help="Verbose logging",
    )
    parser.add_argument(
        "-vv",
        dest="log_level",
        action="store_const",
        const=logging.DEBUG,
        default=logging.WARNING,
        help="Very verbose logging",
    )
    parser.add_argument(
        "--log",
        type=argparse.FileType("w"),
        default="-",
        help="Name of the log file (default logging is to stdout)",
    )
    args = parser.parse_args()

    # Set-up the logging
    logging.basicConfig(
        format="%(asctime)s %(levelname)s:%(message)s",
        datefmt="%Y-%m-%d %H:%M:%S",
        level=args.log_level,
    )

    # Set-up the bot
    config = Config(args.config)
    bot = SkypeBot(config)

    # Ensure CTRL-C is handled
    def signal_handler(sig, frame):
        logging.warning("SIGINT received")
        if bot.is_running():
            logging.warning("Trying to stop gracefully")
            bot.stop()
        else:
            logging.error("Giving up on graceful stop")
            exit(1)

    signal.signal(signal.SIGINT, signal_handler)

    # Run the bot
    bot.run()